---
title: "The Nitty-Gritty of Moving Data with Apache Beam"
date: 2020-09-25T11:33:53-04:00
draft: false
tags: ["mozilla"]
---

_Summary of a talk delivered at Apache Beam Digital Summit on August 24, 2020._

![Title slide](/img/nitty-gritty-beam/title-slide.png)

In this session, you won't learn about joins or windows or timers or any other advanced features of Beam. Instead, we will focus on the real-world complexity that comes from simply moving data from one system to another safely. How do we model data as it passes from one transform to another? How do we handle errors? How do we test the system? How do we organize the code to make the pipeline configurable for different source and destination systems?

We will explore how each of these questions are addressed in [Mozilla's open source codebase for ingesting telemetry data from Firefox clients](https://github.com/mozilla/gcp-ingestion). By the end of the session, you'll be equipped to explore the codebase and documentation on your own to see how these concepts are composed together.

## Links

- [Recording of the talk on YouTube](https://youtu.be/ABWKnl_N550)
- [Session page on beamsummit.org](https://2020.beamsummit.org/sessions/nitty-gritty-moving-data-with-beam/)
- [Slides from the session in Google Docs](https://docs.google.com/presentation/d/19bEXp-OpJ0C0GcqnEuef2ytOX4OnekUFSgArWmOP1Ws/edit?usp=sharing)

<!--more-->
