---
layout: post
title: "A Search for Exotic Particles"
date: 2012-05-07T11:11:11-04:00
comments: true
categories:
- Writing
aliases:
- content/2012/05/07/phd-thesis/
---

My Ph.D. dissertation, performed at the University of Wisconsin-Madison.

Abstract: A search for exotic particles decaying via *WZ* to final states with electrons and muons is performed using a data sample of *pp* collisions collected at 7 TeV center-of-mass energy by the CMS experiment at the LHC, corresponding to an integrated luminosity of 4.98 inverse femtobarns.  A cross section measurement for the production of *WZ* is also performed on a subset of the collision data.  No significant excess is observed over the Standard Model background, so lower bounds at the 95% confidence level are set on the production cross sections of hypothetical particles decaying to *WZ* in several theoretical scenarios.  Assuming the Sequential Standard Model, *W'* bosons with masses below 1143 GeV are excluded.  New limits are also set for several configurations of Low-Scale Technicolor.

<!--more-->

I have put the [LaTeX source on GitHub](https://github.com/jklukas/gradthesis), along with a [style file](https://github.com/jklukas/uwthesis) that should be reuasable for others preparing a UW-Madison dissertation.  I hope others find it helpful as a jumping-off point.

The official version is available as a [record on the CERN Document Server](https://cdsweb.cern.ch/record/1446308).  I also produced a [single-spaced two-column version](/files/klukas-thesis-twocol.pdf) suitable for printing and a [trade size](/files/klukas-thesis-trade.pdf) for ordering a few bound copies on [lulu.com](http://lulu.com).
