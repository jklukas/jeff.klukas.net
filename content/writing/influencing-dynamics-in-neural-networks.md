---
layout: post
title: "Influencing Dynamics in Neural Networks"
date: 2006-04-03T13:33:53-04:00
comments: true
categories:
- Writing
aliases:
- content/2006/04/03/influencing-dynamics-in-neural-networks/
---

This research was performed under an NSF-funded Research Experience for Undergraduates program at Indiana University and then extended at Wittenberg University to serve as an undergraudate honors thesis.  The work was overseen by John Beggs.  It was later published in the 2006 issue of Wittenberg University's non-fiction literary magazine, *Spectrum*.

<!--more-->

Abstract: Experimental readings from rat cortex in our lab have demonstrated a robust ability of neural networks to maintain critical point dynamics. Whether that ability stems from the activity of a regulatory system or is an inherent property of the network, however, remains unknown. Throughout our investigations, a computational model has served both as a useful diagnostic tool in developing measurements of dynamics and as a predictor of manipulations to be in- vestigated in tissue samples. It demonstrates a robustness similar to the biological samples, suggesting that the network structure alone may be sufficient to maintain stable dynamics. Using the model to explore network parameters, we have identified the distribution of connection strengths between nodes in the network as having a clear influence on the dynamical behavior of the system, and further, we have explained deviations from that relationship by identifying particular connection patterns that link attractive behavior with an inverse branching structure.

View the full text of ["Influencing Dynamics in Neural Networks" in PDF format](/files/klukas-undergrad-thesis.pdf).
