---
title: "Deduplication: Where Apache Beam Fits In"
date: 2021-08-04T16:00:00-04:00
draft: false
tags: ["mozilla"]
---

_Summary of a talk delivered at Apache Beam Digital Summit on August 4, 2021._

![Title slide](/img/deduplication-beam/title-slide.png)

This session will start with a brief overview of the problem of duplicate records and the different options available for handling them. We'll then explore two concrete approaches to deduplication within a Beam streaming pipeline implemented in Mozilla’s [open source codebase for ingesting telemetry data](https://github.com/mozilla/gcp-ingestion/tree/main/ingestion-beam) from Firefox clients.

We'll compare the robustness, performance, and operational experience of using the deduplication built in to `PubsubIO` vs. storing IDs in an external Redis cluster and why Mozilla switched from one approach to the other.

Finally, we'll compare streaming deduplication to a [much stronger end-to-end guarantee that Mozilla achieves via nightly scheduled queries](https://github.com/mozilla/bigquery-etl/blob/main/bigquery_etl/copy_deduplicate.py) to serve historical analysis use cases.

## Links

- [Recording of the talk on YouTube](https://www.youtube.com/watch?v=9OfJKDs3h40)
- [Session page on beamsummit.org](https://2021.beamsummit.org/sessions/deduplication/)
- [Slides from the session in Google Docs](https://docs.google.com/presentation/d/1bNOKHw9Gssx_aDI4JilSiqtBaRrEM7SGtygsP9-MgLc/edit?usp=sharing)

<!--more-->
