Hi, I'm Jeff 👋 I live in the Columbus, Ohio area but have been working remotely for various tech companies since 2014.

I tend to poke around Worthington with Karen and our kids 👨‍👩‍👧‍👦 and you can sometimes find me [playing music with some friends 🎵](https://www.chrisbullwinkle.com/) around town.

Professionally, I write software for large-scale data processing 📊 and distributed systems; see all the details on [LinkedIn](https://linkedin.com/in/jeffklukas). I try to speak regularly at local and national conferences, and do some [technical writing ✍️](writing/) at least once a year. I was a collaborator on the Large Hadron Collider 💥 during graduate school and my name is on the [Higgs boson discovery paper](https://www.sciencedirect.com/science/article/pii/S0370269312008581).

I sometimes get the opportunity to contribute directly to some of the open source projects I use.
Within the Apache Kafka ecosystem, see
[KIP-215](https://cwiki.apache.org/confluence/display/KAFKA/KIP-215%3A+Add+topic+regex+support+for+Connect+sinks)
and accepted patches to
[apache/kafka](https://github.com/apache/kafka/pulls?q=author%3Ajklukas)
and
[confluentinc/kafka-connect-storage-cloud](https://github.com/confluentinc/kafka-connect-storage-cloud/pulls?q=author%3Ajklukas+);
I'm also author of
[kafka-dropwizard-reporter](https://github.com/SimpleFinance/kafka-dropwizard-reporter).
Other data processing projects I've worked on include
[apache/beam](https://github.com/apache/beam/pulls?q=author%3Ajklukas+).
Within the PostgreSQL community, see
[pgjdbc](https://github.com/pgjdbc/pgjdbc/pulls?utf8=%E2%9C%93&q=is%3Apr+author%3Ajklukas+).
Within the Python community, I have been a maintainer on
[sqlalchemy-redshift](https://github.com/sqlalchemy-redshift/sqlalchemy-redshift)
and have contributed to
[matplotlib](https://github.com/matplotlib/matplotlib/commit/fcae31a73082acbe9af84578d051591b66a433ea)
and
[sphinx](https://github.com/sphinx-doc/sphinx/search?q=klukas&type=Commits).
